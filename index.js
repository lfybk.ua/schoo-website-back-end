const express = require('express');
const cors = require('cors');

const gallery = require("./router/gallery");
const auth = require("./router/auth");
const diary = require("./router/diary");
const calendar = require("./router/calendar");
const tabs = require("./router/tabs");
const feedback = require("./router/feedback");


const path = require("path")

const cluster = require("cluster")

const addWorker = () => {
  const worker = cluster.fork();

  worker.on('exit', () => {
    console.log(`worker died! Pid ${worker?.process?.pid || worker.pid}`);

    addWorker()
  })
}

if (cluster.isPrimary) {
  addWorker()
} else {
  const app = express();

  const port = process.env.PORT || 5050;

  app.use(cors())
  app.use(express.json());
  app.use(express.urlencoded({
    extended: true
  }));

  app.use('/api/gallery', gallery)
  app.use('/api/auth', auth)
  app.use('/api/diary', diary)
  app.use('/api/calendar', calendar)
  app.use('/api/tabs', tabs)
  app.use('/api/feedback', feedback)

  // app.use('/', express.static(path.join(__dirname, "/view")));

  app.use('**', (req, res) => {
    res.redirect('/')
  })

  app.listen(port, (req, res) => {
    console.log(`Example app listening on port ${port}`)
  })
}
