const nodemailer = require("nodemailer");
const { email } = require("../settings");
const text = require("./text-mail");

let transporter = nodemailer.createTransport(email)


module.exports = async function sendMessage(to) {
  const res = await transporter.sendMail({
    from: '<school@gmail.com>',
    to: to,
    subject: 'Комунальний заклад освіти "НВК № 4 "СЗШ-ДНЗ (ДС)" ДМР',
    html: text,
  })
}


