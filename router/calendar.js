const calendarController = require("../controller/calendar")

const express = require('express');
const router = express.Router();

router.get('/', calendarController.calendarHandler);
router.post('/add', calendarController.add);
router.put('/edit', calendarController.edit);
router.delete('/delete/:id', calendarController.delete);

module.exports = router;