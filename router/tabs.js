const tabsController = require("../controller/tabs")

const express = require('express');
const router = express.Router();

router.get('/', tabsController.tabsHandler);
router.get('/:id', tabsController.pageHandler);

module.exports = router;