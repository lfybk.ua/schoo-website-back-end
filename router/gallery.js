const galleryController = require("../controller/gallery")

const express = require('express');
const router = express.Router();

router.get('/tabs', galleryController.tabsHandler);
router.get('/', galleryController.galleryHandler);

module.exports = router;