const feedbackController = require("../controller/feedback")

const express = require('express');
const router = express.Router();

router.get('/', feedbackController.getAll);
router.post('/set', feedbackController.setFeedback);
router.delete('/delete/:id', feedbackController.deleteFeedback);

module.exports = router;