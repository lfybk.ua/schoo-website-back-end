const diaryController = require("../controller/diary")

const express = require('express');
const router = express.Router();

router.get('/', diaryController.getMark);
router.get('/name', diaryController.getClassName);
router.get('/students/:id', diaryController.getClassStudents);
router.get('/mark/student/:id', diaryController.getMarkStudent);
router.post('/mark', diaryController.addMark);
router.put('/mark/edit', diaryController.editMark);
router.delete('/delete/:id', diaryController.deleteMark);

module.exports = router;