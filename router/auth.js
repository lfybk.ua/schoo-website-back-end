const authController = require("../controller/auth")

const express = require('express');
const router = express.Router();

router.post('/login', authController.loginHandler);
router.get('/admin', authController.admin);
router.get('/users', authController.getUsers);
router.post('/add', authController.add);
router.put('/edit', authController.edit);
router.delete('/delete/:id', authController.delete);

module.exports = router;