const model = new (require("../model/tabs"))();



module.exports = {
  async tabsHandler(req, res) {
    const [table, fields] = await model.getTabs()

    res.json(table);
  },
  async pageHandler(req, res) {
    const [table, fields] = await model.getPage(req.params.id)

    res.json(table);
  }
};
