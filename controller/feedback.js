const { verify } = require("./JWT/token");
const model = new (require("../model/feedback"))();

const sendMail = require("../util/email")

const isAuth = (req) => {
  if (!req.headers.authorization) throw { status: 400, type: "key", error: "Invalid key" };

  const decoded = verify(req.headers.authorization);

  if (!decoded?.data?.email) throw { status: 400, type: "key", error: "Invalid key" };

  return decoded;
}

module.exports = {
  async setFeedback(req, res) {
    try {
      await model.set(req.body);

      console.log(req.body.email);
      sendMail(req.body.email).catch(e => {
        console.log(e);
      });

      res.json({ status: "OK" })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  }, async getAll(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      const [data, dataAdmin] = await model.get();

      res.json(data)
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({

        error: error,
        type: type
      })
    }
  },
  async deleteFeedback(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      await model.delete(req.params.id);

      res.json({ status: "OK" })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({

        error: error,
        type: type
      })
    }
  },
}
