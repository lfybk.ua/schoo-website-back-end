const { sign, verify } = require("./JWT/token");
const { verify: verifyPassword, hash } = require("./crypto/hash");
const model = new (require("../model/auth"))();

const isAuth = (req) => {
  if (!req.headers.authorization) throw { status: 400, type: "key", error: "Invalid key" };

  const decoded = verify(req.headers.authorization);

  if (!decoded?.data?.email) throw { status: 400, type: "key", error: "Invalid key" };

  return decoded;
}

module.exports = {
  async loginHandler(req, res) {
    try {
      const [table, fields] = await model.getLogin(req.body.email);

      if (!table[0]) throw { error: "Email is not valid", status: 404, type: "email" };

      const [data] = table;

      if (!await verifyPassword(req.body.password, data.salt, data.password)) throw { error: "Password is not valid", status: 400, type: "password" };

      const key = sign({ email: req.body.email });

      res.json({
        role: data.role,
        phone: data.phone,
        fullName: data.fullName,
        key: key,
      });
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async admin(req, res) {
    try {
      const decoded = isAuth(req);

      const [table, fieldsTable] = await model.getLogin(decoded.data.email);

      const [data] = table;

      const key = sign({ email: decoded.data.email });

      res.json({
        role: data.role,
        phone: data.phone,
        fullName: data.fullName,
        key: key,
      });
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async getUsers(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      const [data, fieldsData] = await model.getUsers();

      res.json(data)
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async add(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      const { password, salt } = await hash(req.body.password);

      await model.add({
        password: password,
        email: req.body.email,
        salt: salt,
        phone: req.body.phone,
        role: req.body.role,
        fullName: req.body.fullName
      });

      res.json({ status: "OK" })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async edit(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      let str = '';

      if (req.body.email) {
        str += `email = '${req.body.email}'`
      }
      if (req.body.phone) {
        str += str ? ", " : ""
        str += `phone = '${req.body.phone}'`
      }
      if (req.body.fullName) {
        str += str ? ", " : ""
        str += `fullName = '${req.body.fullName}'`
      }
      if (req.body.role) {
        str += str ? ", " : ""
        str += `role = '${req.body.role}'`
      }
      if (req.body.password) {
        str += str ? ", " : ""
        const { password, salt } = await hash(req.body.password);

        str += `password = '${password}', salt = '${salt}'`
      }

      await model.edit(req.body.id, str);

      res.json({ status: "OK" })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async delete(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }
      true
      await model.delete(req.params.id);

      res.json({ status: "OK" })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({

        error: error,
        type: type
      })
    }
  }
};