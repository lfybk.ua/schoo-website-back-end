const model = new (require("../model/gallery"))();



module.exports = {
  async tabsHandler(req, res) {
    const [table, fields] = await model.getTabs()

    res.json(table);
  },
  async galleryHandler(req, res) {
    const [table, fields] = await model.getGallery(req.query.id)

    res.json(table);
  }
};

// class Gallery {
//   constructor() {
//     this.model = new GalleryModel()
//   }

//   async tabsHandler(req, res) {
//     const [table, fields] = await this.model.getTabs()

//     res.json(table);
//   }
// }