const jwt = require("jsonwebtoken");
const { secretJWT } = require("../../util/const");

module.exports = {
  sign(data) {
    return jwt.sign(
      {
        data: data
      },
      secretJWT,
      {
        expiresIn: '1d'
      }
    );
  },
  verify(token) {
    try {
      return jwt.verify(
        token,
        secretJWT,
      );
    } catch (err) {
      return null;
    }
  }
}