const crypto = require("crypto")

function hash(password) {
  return new Promise((resolve, reject) => {
    const salt = crypto.randomBytes(8).toString("hex")

    crypto.scrypt(password, salt, 64, (err, derivedKey) => {
      if (err) reject(err);
      resolve({ salt: salt, password: derivedKey.toString('hex') })
    });
  })
}

function verify(password, salt, key) {
  return new Promise((resolve, reject) => {
    crypto.scrypt(password, salt, 64, (err, derivedKey) => {
      if (err) reject(err);

      resolve(key.trim() == derivedKey.toString('hex'))
    });
  })
}

module.exports = { verify, hash };
