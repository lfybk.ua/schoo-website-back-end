const model = new (require("../model/calendar"))();
const { verify } = require("./JWT/token");

const isAuth = (req) => {
  if (!req.headers.authorization) throw { status: 400, type: "key", error: "Invalid key" };

  const decoded = verify(req.headers.authorization);

  if (!decoded?.data?.email) throw { status: 400, type: "key", error: "Invalid key" };

  return decoded;
}

module.exports = {
  async calendarHandler(req, res) {
    const [table, fields] = await model.getDate()

    res.json(table);
  },
  async add(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      await model.addDate({
        event: req.body.event,
        date: req.body.date,
      });

      res.json({
        status: "OK"
      })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async edit(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      let str = "";

      if (req.body.event) {
        str += `event = '${req.body.event}'`
      }
      if (req.body.date) {
        str += str ? ", " : ""
        str += `date = '${req.body.date}'`
      }
      await model.editDate(str, req.body.id);

      res.json({
        status: "OK"
      })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async delete(req, res) {
    try {
      const decoded = isAuth(req);

      const [admin, fieldsAdmin] = await model.isAdmin(decoded.data.email);

      if (admin?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      await model.deleteDate(req.params.id);

      res.json({
        status: "OK"
      })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
};
