const { verify } = require("./JWT/token");
const model = new (require("../model/diary"))();

const isAuth = (req) => {
  if (!req.headers.authorization) throw { status: 400, type: "key", error: "Invalid key" };

  const decoded = verify(req.headers.authorization);

  if (!decoded?.data?.email) throw { status: 400, type: "key", error: "Invalid key" };

  return decoded;
}

module.exports = {
  async getMark(req, res) {
    try {
      const decoded = isAuth(req)

      const [mark, fieldsMark] = await model.getMark(decoded.data.email);
      const [studentClass, fieldsClass] = await model.getClass(decoded.data.email);

      res.json({
        mark: mark,
        class: studentClass
      });
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async getClassName(req, res) {
    try {
      const decoded = isAuth(req);

      const [teacher, fieldsTeacher] = await model.isTeacher(decoded.data.email);

      if (teacher?.[0]?.role !== "teacher" && teacher?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      const [data, fieldsData] = await model.getClassName();

      res.json(data)
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async getClassStudents(req, res) {
    try {
      const decoded = isAuth(req);

      const [teacher, fieldsTeacher] = await model.isTeacher(decoded.data.email);

      if (teacher?.[0]?.role !== "teacher" && teacher?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      const [data, fieldsData] = await model.getClassStudents(req.params.id);

      res.json(data)
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async getMarkStudent(req, res) {
    try {
      const decoded = isAuth(req);

      const [teacher, fieldsTeacher] = await model.isTeacher(decoded.data.email);

      if (teacher?.[0]?.role !== "teacher" && teacher?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      const [mark, fieldsMark] = await model.getMarkStudent(req.params.id);
      const [subject, fieldsSubject] = await model.getSubject();

      res.json({
        mark: mark,
        subject: subject,
      })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async addMark(req, res) {
    try {
      const decoded = isAuth(req);

      const [teacher, fieldsTeacher] = await model.isTeacher(decoded.data.email);

      if (teacher?.[0]?.role !== "teacher" && teacher?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      await model.addMark(req.body);

      res.json({ status: "OK" })
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async editMark(req, res) {
    try {
      const decoded = isAuth(req);

      const [teacher, fieldsTeacher] = await model.isTeacher(decoded.data.email);

      if (teacher?.[0]?.role !== "teacher" && teacher?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      let str = '';

      if (req.body.mark || req.body.mark === null || req.body.mark === 0) str += `mark = ${req.body.mark ? req.body.mark : '0'}`
      if (req.body.text || req.body.text === null || req.body.text === "") {
        str += str ? ", " : ""
        str += `text = ${req.body.text ? "'" + req.body.text + "'" : 'null'}`
      }
      if (req.body.idSubject) {
        str += str ? ", " : ""
        str += `idSubject = ${req.body.idSubject}`
      }
      if (req.body.date) {
        str += str ? ", " : ""
        str += `date = '${req.body.date}'`
      }

      await model.editMark(req.body.id, str);

      res.json({ status: "OK" })

    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
  async deleteMark(req, res) {
    try {
      const decoded = isAuth(req);

      const [teacher, fieldsTeacher] = await model.isTeacher(decoded.data.email);

      if (teacher?.[0]?.role !== "teacher" && teacher?.[0]?.role !== "admin") {
        return res.redirect("/")
      }

      await model.deleteMark(req.params.id);

      res.json({ status: "OK" })

    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  }
};