const mysql = require("mysql2");
const { dev } = require("../settings");

const addPull = () => {
  const pull = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    database: 'school',
    password: "",
    waitForConnections: true,
    connectionLimit: 1,
    queueLimit: 0
  });
  return () => pull
}



module.exports = addPull();