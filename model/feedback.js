const addPull = require("./connect");

class feedbackModel {
  constructor() {
    this.pull = addPull();
  }

  async get() {
    return this.pull.promise().query(
      `SELECT * FROM feedback;`
    );
  }

  async set({ email, phone, fullName }) {
    return this.pull.promise().query(
      `INSERT INTO feedback (\`email\`, \`phone\`, \`fullName\`, \`id\`) VALUES ('${email}', '${phone}', '${fullName}', NULL);`
    );
  }

  async delete(id) {
    return this.pull.promise().query(
      `DELETE FROM feedback WHERE feedback.id = ${id};`
    );
  }

  async isAdmin(email) {
    return this.pull.promise().query(
      `SELECT * FROM login WHERE login.email='${email}';`
    );
  }
}

module.exports = feedbackModel;