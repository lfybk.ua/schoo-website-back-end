const addPull = require("./connect");


class TabsModel {
  constructor() {
    this.pull = addPull();
    this.tableNameTabs = "tabs"
    this.tableNamePage = "pagetabs"
  }

  async getTabs() {
    return this.pull.promise().query("SELECT * FROM `" + this.tableNameTabs + "`");
  }

  async getPage(id) {
    return this.pull.promise().query(
      `SELECT * FROM \`${this.tableNamePage}\` WHERE idTabs = ${id}`);
  }
}

module.exports = TabsModel;