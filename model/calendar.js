const addPull = require("./connect");

class CalendarModel {
  constructor() {
    this.pull = addPull();

    this.calendarNameTabs = "calendar"
  }

  async getDate() {
    return await this.pull.promise().query("SELECT * FROM `" + this.calendarNameTabs + "` ORDER BY date");
  }

  async addDate({ date, event }) {
    return await this.pull.promise().query(
      `INSERT INTO \`calendar\` (\`id\`, \`date\`, \`event\`) VALUES (NULL, '${date}', '${event}');`);
  }

  async editDate(data, id) {
    return await this.pull.promise().query(
      `UPDATE \`calendar\` SET ${data} WHERE \`calendar\`.\`id\` = ${id};`);
  }

  async deleteDate(id) {
    return await this.pull.promise().query(
      "DELETE FROM `calendar` WHERE `calendar`.`id` = " + id + ";");
  }

  async isAdmin(email) {
    return this.pull.promise().query(
      `SELECT * FROM login WHERE login.email='${email}';`
    );
  }
}

module.exports = CalendarModel;