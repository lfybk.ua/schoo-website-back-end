const addPull = require("./connect");


class GalleryModel {
  constructor() {
    this.pull = addPull();
    this.tableNameTabs = "gallerytabs"
    this.tableNameGallery = "gallery"
  }

  async getTabs() {
    return this.pull.promise().query("SELECT * FROM `" + this.tableNameTabs + "`");
  }

  async getGallery(id) {
    return this.pull.promise().query(
      `SELECT * FROM \`${this.tableNameGallery}\` WHERE idGallery = ${id}`);
  }
}

module.exports = GalleryModel;