const addPull = require("./connect");

class AuthModel {
  constructor() {
    this.pull = addPull();
    this.tableNameLogin = "login"
  }

  async getLogin(email) {
    return await this.pull.promise().query(
      `SELECT * FROM \`${this.tableNameLogin}\` where email IN ('${email}')`);
  }

  async getUsers(email) {
    return await this.pull.promise().query(
      `SELECT email, phone, role, fullName, id FROM \`${this.tableNameLogin}\``);
  }

  async add({ password, email, salt, phone, role, fullName }) {
    return await this.pull.promise().query(
      `INSERT INTO login (\`id\`, \`password\`, \`email\`, \`salt\`, \`phone\`, \`role\`, \`fullName\`) VALUES (NULL, '${password}', '${email}', '${salt}', '${phone}', '${role}', '${fullName}');`);
  }

  async edit(id, data) {
    return await this.pull.promise().query(
      `UPDATE login SET ${data} WHERE login.id=${id};`);
  }

  async delete(id) {
    return await this.pull.promise().query(
      `DELETE FROM login WHERE id=${id};`);
  }

  async isAdmin(email) {
    return this.pull.promise().query(
      `SELECT * FROM login WHERE login.email='${email}';`
    );
  }
}

module.exports = AuthModel;