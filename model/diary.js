const addPull = require("./connect");

class diaryModel {
  constructor() {
    this.pull = addPull();

    this.tableNameMark = "mark"
    this.tableNameLogin = "login"
    this.tableNameSubject = "subject"
  }

  async getMark(email) {
    return this.pull.promise().query(
      `SELECT mark.mark, mark.text, mark.date, subject.subjectName FROM \`mark\` LEFT JOIN login on mark.idStudent=login.id LEFT JOIN subject on subject.id=mark.idSubject WHERE login.email in ('${email}') ORDER BY date DESC;`
    );
  }

  async getClass(email) {
    return this.pull.promise().query(
      `SELECT login.fullName FROM \`class\` LEFT JOIN login on login.id=class.idStudent WHERE class.className in (SELECT class.className FROM \`class\` LEFT JOIN login on login.id=class.idStudent WHERE login.email in ('${email}'));`
    );
  }

  async getClassName() {
    return this.pull.promise().query(
      `SELECT class.className, class.idStudent FROM \`class\`;`
    );
  }

  async getClassStudents(classId) {
    return this.pull.promise().query(
      `SELECT login.fullName, login.id, login.email, login.phone FROM login LEFT JOIN class on login.id=class.idStudent WHERE class.className in('${classId}');`
    );
  }

  async getMarkStudent(idStudent) {
    return this.pull.promise().query(
      `SELECT * FROM mark WHERE mark.idStudent in (${idStudent});`
    );
  }

  async addMark({ idStudent, mark, text, idSubject, date }) {
    return this.pull.promise().query(
      `INSERT INTO mark VALUES (null, ${idStudent}, ${mark ? mark : '0'}, ${text ? `'${text}'` : 'null'}, ${idSubject}, '${date}');`
    );
  }

  async editMark(id, data) {
    return this.pull.promise().query(
      `UPDATE mark SET ${data} WHERE mark.id=${id};`
    );
  }

  async deleteMark(id) {
    return this.pull.promise().query(
      `DELETE FROM mark WHERE id=${id};`
    );
  }

  async isTeacher(email) {
    return this.pull.promise().query(
      `SELECT * FROM login WHERE login.email='${email}';`
    );
  }

  async getSubject() {
    return this.pull.promise().query(
      `SELECT * FROM subject;`
    );
  }
}

module.exports = diaryModel;